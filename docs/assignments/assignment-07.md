# Assignment 7
1. Use the `fetch`-API to connect your nickname selector to the development REST backend. Use the following resource endpoints for this:
    - enter chatroom: POST to `/api/users` with `Content-Type: application/json` and body `{"nickname": "<value>"}`
    - leave chatroom: DELETE to `/api/users/{userId}`. The `userId` can be retrieved from the user object from the response of create-user.
    
    Give feedback to the user, while entering/leaving the chatroom is in progress. (Hint: use bandwidth throttling of your browser debugging tools, to check if this works.)
    
2. Extract the `fetch`-calls into a separate _service_ module to decouple it from the react component.
3. Use a `model` class for the user object, to decouple the component from the REST API responses and have object conversions in one place.
4. Optional: Implement a new user list component, listing the current members of the chatroom.